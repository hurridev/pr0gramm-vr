﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Video;
using Valve.VR;
using Valve.VR.Extras;

public class GetImages : MonoBehaviour
{
    public SteamVR_LaserPointer laserPointer;
    public GameObject thumbnailPrefab;
    public GameObject player;
    public GameObject activeItem;

    public GameObject overlay;

    public GameObject toggleSlideshow;

    public Text plus;
    public Text minus;
    public Text comments;

    public Text usernameLabel;
    public Text benisLabel;


    public GameObject topToggle;
    public GameObject sfwToggle;
    public GameObject nsfwToggle;
    public GameObject nsflToggle;

    public GameObject upVoteToggle;
    public GameObject downVoteToggle;

    private Setting settings;
    private Api api;

    private List<GameObject> quads { get; set; }
    private Color orange;

    private List<Item> feedItems;
    private int activeItemIndex = 0;

    private Coroutine slideshowCoroutine;
    float length;

    void Awake()
    {
        laserPointer.PointerIn += PointerInside;
        laserPointer.PointerOut += PointerOutside;
        laserPointer.PointerClick += PointerClick;

        SteamVR_Actions.default_AButton.AddOnChangeListener(aButtonPressed, SteamVR_Input_Sources.RightHand);
        SteamVR_Actions.default_BButton.AddOnChangeListener(bButtonPressed, SteamVR_Input_Sources.RightHand);
    }


    // Start is called before the first frame update
    void Start()
    {
        ColorUtility.TryParseHtmlString("#ee4d2e", out orange);


        settings = Settings.Default.get();

        api = new Api();
        api.showTop = topToggle.GetComponent<Toggle>().isOn;
        api.sfwActive = sfwToggle.GetComponent<Toggle>().isOn;
        api.nsfwActive = nsfwToggle.GetComponent<Toggle>().isOn;
        api.nsflActive = nsflToggle.GetComponent<Toggle>().isOn;

        api.setAuthCookie(settings.authCookie);

        FetchData();
    }

    private void reload()
    {
        Debug.Log("Reloading");
        quads.ForEach(quad =>
        {
            Destroy(quad);
        });

        hideItem();
        FetchData();
    }

    async void FetchData()
    {
        //bool isLoggedIn = api.isLoggedIn();
        //Debug.Log("You are logged in: " + isLoggedIn);

        UserInfo userInfo = await api.getUserInfo(api.getUsername());
        usernameLabel.text = userInfo.user.name;
        benisLabel.text = "Benis: " + userInfo.user.score;

        RootObject feed = await api.getItemsAsync();
        feedItems = feed.items;

        int count = feedItems.Count;

        quads = new List<GameObject>(count);
        addFeedItemsToScene(feedItems);
    }

    private void addFeedItemsToScene(List<Item> feedItems, int offset = 0)
    {
        for (int i = 0; i < feedItems.Count; i++)
        {
            int absoluteIndex = i + offset;
            int col = absoluteIndex % 20;
            int row = (int)Math.Floor(absoluteIndex / 20.0);
            float spacing = 0.1f;

            GameObject quad = GameObject.Instantiate(thumbnailPrefab);
            quad.name = "" + feedItems[i].id;
            quad.transform.position = new Vector3(col + (col * spacing), -row - (row * spacing), 0);
            quad.GetComponent<Renderer>().material.color = orange;
            quads.Add(quad);
        }

        StartCoroutine(loadThumbnails(feedItems, offset));
    }


    public void StartSlideshow()
    {
        slideshowCoroutine = StartCoroutine(Slideshow());

    }
    public void StopSlideshow()
    {
        if (slideshowCoroutine != null)
        {
            StopCoroutine(slideshowCoroutine);
            slideshowCoroutine = null;
        }
    }
    public bool isSlideShowActive()
    {
        return slideshowCoroutine != null;
    }

    private IEnumerator Slideshow()
    {

        for (int i = 0; i < feedItems.Count; i++)
        {
            Item currentItem = feedItems[activeItemIndex++];

            Debug.Log("Showing " + i);
            yield return showItem(currentItem);

            yield return new WaitForSeconds(length);
        }
    }


    private List<Comment> getChildrenOf(List<Comment> comments, Comment comment)
    {
        List<Comment> list = new List<Comment>();

        return comments.FindAll(c => c.parent == comment.id);
    }

    private string recursiveComment(List<Comment> comments, Comment comment, int i)
    {

        string tabs = String.Concat(Enumerable.Repeat(" ", 5 * i));

        string content = comment.content.Replace("\n", tabs + "\n");
        string s = tabs + comment.name + ": " + content + "\n";

        List<Comment> childComments = getChildrenOf(comments, comment);
        for (int j = 0; j < childComments.Count; j++)
        {
            Comment childComment = childComments[j];
            s += recursiveComment(comments, childComment, i + 1);
        }

        return s;
    }

    private IEnumerator showItem(Item currentItem)
    {
        string image = "https://img.pr0gramm.com/" + currentItem.image;

        bool isVideo = image.EndsWith(".mp4"); // TODO there are also other formats like .gif


        plus.text = "+ " + currentItem.up;
        minus.text = "- " + currentItem.down;
        comments.text = "";
        upVoteToggle.GetComponent<Toggle>().SetIsOnWithoutNotify(false);
        downVoteToggle.GetComponent<Toggle>().SetIsOnWithoutNotify(false);

        string commentsText = "";
        Task a = Task.Run(async () =>
        {
            ItemInfo itemInfo = await api.getInfoFor(currentItem);

            List<Comment> commentsList = itemInfo.comments;

            for (int i = 0; i < commentsList.Count; i++)
            {
                Comment comment = commentsList[i];
                if (comment.parent != 0) continue;

                commentsText += recursiveComment(commentsList, comment, 0);
            }
        });

        while (!a.IsCompleted) yield return new WaitForSeconds(0.1f); ;

        comments.text = commentsText;

        UnityWebRequest www2 = UnityWebRequestTexture.GetTexture(image);
        yield return www2.SendWebRequest();

        if (www2.isNetworkError || www2.isHttpError)
        {
            Debug.Log(www2.error);
        }
        else
        {
            activeItem.SetActive(true);
            Texture thumbnailTexture = ((DownloadHandlerTexture)www2.downloadHandler).texture;
            Material mat = activeItem.GetComponent<Renderer>().material;
            mat.color = Color.white;
            mat.mainTexture = thumbnailTexture;

            bool isHorizontal = currentItem.width > currentItem.height;
            float factor = 1.0f;

            if (isHorizontal)
            {
                factor = 10.0f / currentItem.width;
            }
            else
            {
                factor = 10.0f / currentItem.height;
            }

            float width = currentItem.width * factor;
            float height = currentItem.height * factor;
            activeItem.transform.localScale = new Vector3(width, height, 1);

            VideoPlayer video = activeItem.GetComponent<VideoPlayer>();
            video.SetTargetAudioSource(0, activeItem.GetComponent<AudioSource>());
            length = 5.0f;
            if (isVideo)
            {
                video.url = image;
                video.playOnAwake = true;
                activeItem.GetComponent<VideoPlayer>().enabled = true;

                while (!video.isPrepared) yield return new WaitForSeconds(0.1f);
                length = (float)video.length;
            }
            else
            {
                activeItem.GetComponent<VideoPlayer>().enabled = false;
            }


        }
    }

    private void hideItem()
    {
        activeItem.SetActive(false);
    }

    private IEnumerator loadThumbnails(List<Item> items, int offset)
    {

        for (int i = 0; i < items.Count; i++)
        {
            Debug.Log(i + ". Getting id: " + items[0].id);

            string thumbnail = "https://thumb.pr0gramm.com/" + items[i].thumb;
            string image = "https://img.pr0gramm.com/" + items[i].image;

            UnityWebRequest www2 = UnityWebRequestTexture.GetTexture(thumbnail);
            yield return www2.SendWebRequest();

            if (www2.isNetworkError || www2.isHttpError)
            {
                Debug.LogError("Could not send request: " + www2.error);
            }
            else
            {
                Texture thumbnailTexture = ((DownloadHandlerTexture)www2.downloadHandler).texture;
                Material mat = quads[i + offset].GetComponent<Renderer>().material;
                mat.color = Color.white;
                mat.mainTexture = thumbnailTexture;
            }
        }
    }



    // Update is called once per frame
    void Update()
    {
        Vector2 scroll = SteamVR_Input.GetVector2("Scroll", SteamVR_Input_Sources.RightHand);
        player.transform.position += Vector3.down * Time.deltaTime * 5 * -scroll.y;

        overlay.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, -2);
    }



    private async void aButtonPressed(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource, bool newState)
    {
        if (!newState) return;
        activeItemIndex++;

        if (activeItemIndex >= feedItems.Count - 1)
        {
            RootObject feed = await api.getItemsAsync(feedItems[feedItems.Count - 1]);
            addFeedItemsToScene(feed.items, feedItems.Count);
            feedItems.AddRange(feed.items);
        }

        Item currentItem = feedItems[activeItemIndex];

        Debug.Log("Showing " + activeItemIndex);
        StartCoroutine(showItem(currentItem));
    }

    private void bButtonPressed(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource, bool newState)
    {
        Debug.Log("b changed: " + newState + "---" + fromAction.changed);
        if (!newState) return;
        activeItemIndex--;
        Item currentItem = feedItems[activeItemIndex];

        Debug.Log("Showing " + activeItemIndex);
        StartCoroutine(showItem(currentItem));
    }

    public void PointerClick(object sender, PointerEventArgs e)
    {
        // clicked
        changeColorIf3dObject(e.target, Color.red);

        GameObject target = e.target.gameObject;

        Debug.Log("Clicked object '" + e.target.name + "'.");
        if (target == activeItem)
        {
            hideItem();
        }
        else if (target == toggleSlideshow)
        {
            if (isSlideShowActive())
            {
                StopSlideshow();
            }
            else
            {
                StartSlideshow();
            }
        }
        else if (target == topToggle || target == sfwToggle || target == nsfwToggle || target == nsflToggle || target == upVoteToggle || target == downVoteToggle)
        {
            Toggle toggle = target.GetComponent<Toggle>();
            toggle.isOn = !toggle.isOn;
        }
        else
        {
            int index = quads.IndexOf(target);
            activeItemIndex = index;

            Item currentItem = feedItems[index];
            StartCoroutine(showItem(currentItem));
        }

    }

    public void ToggleChanged(bool filterChanged)
    {
        Debug.Log("Toggle changed");

        if (filterChanged)
        {
            api.showTop = topToggle.GetComponent<Toggle>().isOn;
            api.sfwActive = sfwToggle.GetComponent<Toggle>().isOn;
            api.nsfwActive = nsfwToggle.GetComponent<Toggle>().isOn;
            api.nsflActive = nsflToggle.GetComponent<Toggle>().isOn;

            reload();
        }
        else
        {
            // Voted
            // TODO this triggers 2 times, if toggleGroup gets active (deslects one and selects other one)
            bool up = upVoteToggle.GetComponent<Toggle>().isOn;
            bool down = downVoteToggle.GetComponent<Toggle>().isOn;
            int blussis = up ? 1 : -1;

            if (!up && !down) blussis = 0;

            api.vote(feedItems[activeItemIndex], blussis);
        }
    }

    public void PointerInside(object sender, PointerEventArgs e)
    {
        // hover start
        if (e.target.gameObject != activeItem)
        {
            changeColorIf3dObject(e.target, Color.yellow);
        }
    }

    public void PointerOutside(object sender, PointerEventArgs e)
    {
        // hover end
        changeColorIf3dObject(e.target, Color.white);
    }

    private void changeColorIf3dObject(Transform transform, Color color)
    {
        Renderer renderer;
        bool hasRenderer = transform.TryGetComponent<Renderer>(out renderer);
        if (hasRenderer)
        {
            renderer.material.color = color;
        }
    }
}
