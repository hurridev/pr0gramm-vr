﻿using UnityEngine;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System;

public class Api
{
    private const byte SFW = 0x01 + 0x08;
    private const byte NSFW = 0x02;
    private const byte NSFL = 0x04;

    public bool showTop { get; set; }

    private string authCookie;

    private bool authenticated;

    public bool sfwActive { get; set; }
    public bool nsfwActive { get; set; }
    public bool nsflActive { get; set; }

    private UserCookie userCookie;

    public void setAuthCookie(string authCookie)
    {
        this.authCookie = authCookie;

        try
        {
            string encodedMe = authCookie.Substring(authCookie.IndexOf("me=") + 3);
            string decodedMe = HttpUtility.UrlDecode(encodedMe);
            userCookie = JsonUtility.FromJson<UserCookie>(decodedMe);
        }
        catch (Exception e)
        {
            Debug.LogError("Could not decode given cookie. Authentication will not work. Error " + e);
        }
    }

    public bool isLoggedIn()
    {
        // TODO not sure how to test if cookie is valid
        return false;
    }

    private async Task<T> getRequest<T>(string url)
    {
        Debug.Log("Calling URL '" + url + "'.");

        WebClient webClient = new WebClient();
        webClient.Headers.Add(HttpRequestHeader.Cookie, authCookie);
        string json = await webClient.DownloadStringTaskAsync(url);
        T feed = JsonUtility.FromJson<T>(json);

        return feed;
    }

    private async Task<T> postRequest<T>(string url, string body)
    {
        Debug.Log("Calling URL '" + url + "' with data '" + body + "'.");

        WebClient webClient = new WebClient();
        webClient.Headers.Add(HttpRequestHeader.Cookie, authCookie);
        webClient.Headers.Add(HttpRequestHeader.ContentType, "application/x-www-form-urlencoded; charset=UTF-8");
        string json = await webClient.UploadStringTaskAsync(new System.Uri(url), body);

        T feed = JsonUtility.FromJson<T>(json);
        return feed;
    }

    public async Task<RootObject> getItemsAsync(Item olderThanItem = null)
    {

        int flags = 0x00;

        if (sfwActive) flags |= SFW;
        if (nsfwActive) flags |= NSFW;
        if (nsflActive) flags |= NSFL;

        string url = "https://pr0gramm.com/api/items/get?flags=" + flags;
        url += "&promoted=" + (showTop ? "1" : "0");
        //url += "&tags=!+s%3A1000"; // Benis > 1k

        if (olderThanItem != null)
        {
            int id = olderThanItem.id;
            if (showTop)
            {
                id = olderThanItem.promoted;
            }
            url += "&older=" + id;
        }

        RootObject rootObject = await getRequest<RootObject>(url);
        return rootObject;
    }

    public string getUsername()
    {
        return userCookie.n;
    }

    public async Task<ItemInfo> getInfoFor(Item item)
    {
        string url = "https://pr0gramm.com/api/items/info?itemId=" + item.id;
        ItemInfo itemInfo = await getRequest<ItemInfo>(url);
        return itemInfo;
    }

    public async Task<UserInfo> getUserInfo(string username)
    {
        string url = "https://pr0gramm.com/api/profile/info?name=" + username + "&flags=9";
        UserInfo feed = await getRequest<UserInfo>(url);
        return feed;
    }

    public async void vote(Item item, int blussis)
    {
        string nonce = userCookie.id.Substring(0, 16);

        string url = "https://pr0gramm.com/api/items/vote";
        string content = "id=" + item.id + "&vote=" + blussis + "&_nonce=" + nonce;

        await postRequest<ItemInfo>(url, content);
    }
}
